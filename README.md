# Registry Server

This is a microsevice. Used as component of Hospital Registry application with basic hospital registry functionality.
<br/>
The microsevice provides business logic functionality for the application.

Running application link: http://194.31.174.36:9099/
<br/>
DockerHub - https://hub.docker.com/repositories/rloutsker
<br/>
Documentation file- [hospital-registry-docs.pdf](https://gitlab.com/devsphere/hospital-registry/registry-server/-/blob/master/Documentation_public_.pdf)