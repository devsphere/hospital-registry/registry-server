CREATE TABLE IF NOT EXISTS insurance_policy (
    id SERIAL PRIMARY KEY,
    code CHARACTER(12) NOT NULL,
    registration_date DATE NOT NULL,
    expiration_date DATE NOT NULL
);

CREATE TABLE IF NOT EXISTS patient (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    lastname VARCHAR(255) NOT NULL,
    email VARCHAR(255) UNIQUE NOT NULL,
    birth_date DATE NOT NULL,
    employment VARCHAR(255) NOT NULL,
    policy_id INTEGER NOT NULL REFERENCES insurance_policy(id)
        ON DELETE CASCADE
        ON UPDATE RESTRICT
);

CREATE TABLE IF NOT EXISTS doctor (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    lastname VARCHAR(255) NOT NULL,
    email VARCHAR(255) UNIQUE NOT NULL,
    birth_date DATE NOT NULL,
    employment_date DATE NOT NULL,
    specialization VARCHAR(255) NOT NULL,
    cabinet INTEGER UNIQUE
);

CREATE TABLE IF NOT EXISTS referral (
    patient_id INTEGER NOT NULL REFERENCES patient(id)
        ON DELETE CASCADE
        ON UPDATE RESTRICT,
    doctor_id INTEGER NOT NULL REFERENCES doctor(id)
        ON DELETE CASCADE
        ON UPDATE RESTRICT,
    appointment_date DATE NOT NULL,
    appointment_time TIME NOT NULL,
    primary key (patient_id, doctor_id, appointment_date, appointment_time)
);

CREATE TABLE IF NOT EXISTS manager (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    lastname VARCHAR(255) NOT NULL,
    email VARCHAR(255) UNIQUE NOT NULL,
    birth_date DATE NOT NULL,
    employment_date DATE NOT NULL,
    department varchar(255) NOT NULL
);
