package com.javapadawan.RegistryServer.repository;

import com.javapadawan.RegistryServer.entity.domain.InsurancePolicy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PolicyRepository extends JpaRepository<InsurancePolicy, Long> {
    @Query("SELECT ip FROM insurance_policy AS ip ORDER BY ip.id DESC LIMIT 1")
    InsurancePolicy findTheLastOne();
}
