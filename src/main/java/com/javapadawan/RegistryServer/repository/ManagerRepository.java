package com.javapadawan.RegistryServer.repository;

import com.javapadawan.RegistryServer.entity.domain.Manager;
import org.springframework.stereotype.Repository;

@Repository
public interface ManagerRepository extends PersonBasedRepository<Manager> {
}
