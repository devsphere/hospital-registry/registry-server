package com.javapadawan.RegistryServer.repository;

import com.javapadawan.RegistryServer.entity.domain.Referral;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ReferralRepository extends JpaRepository<Referral, Long> {
}
