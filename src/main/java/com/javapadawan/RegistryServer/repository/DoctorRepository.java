package com.javapadawan.RegistryServer.repository;

import com.javapadawan.RegistryServer.entity.domain.Doctor;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DoctorRepository extends PersonBasedRepository<Doctor> {
    List<Doctor> findAll();
    void deleteByEmail(String email);
    boolean existsByCabinet(int number);
}
