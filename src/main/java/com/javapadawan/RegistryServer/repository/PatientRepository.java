package com.javapadawan.RegistryServer.repository;

import com.javapadawan.RegistryServer.entity.domain.Patient;
import org.springframework.stereotype.Repository;

@Repository
public interface PatientRepository extends PersonBasedRepository<Patient> {
}
