package com.javapadawan.RegistryServer.repository;

import com.javapadawan.RegistryServer.entity.domain.Person;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
@Primary
public interface PersonBasedRepository<T extends Person> extends JpaRepository<T, Long> {
    T findByEmail(String email);
    boolean existsByEmail(String email);
    List<T> findAll();
}
