package com.javapadawan.RegistryServer.web.controller;

import com.javapadawan.RegistryServer.entity.domain.Doctor;
import com.javapadawan.RegistryServer.entity.payload.DoctorDto;
import com.javapadawan.RegistryServer.exception.DomainException;
import com.javapadawan.RegistryServer.service.logic.PersonManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/logic/doctor")
public class DoctorController {
    private final PersonManager service;

    @Autowired
    public DoctorController(PersonManager service) {
        this.service = service;
    }

    @GetMapping("/getById/{id}")
    public Doctor getById(@PathVariable long id) {
        try {
            return service.getDoctorById(id);
        } catch (Exception ex) {
            throw new DomainException(ex.getMessage());
        }
    }

    @PostMapping("/save")
    public void addDoctor(@RequestBody DoctorDto dto) {
        try {
            service.saveDoctorFromDto(dto);
        } catch (Exception ex) {
            throw new DomainException(ex.getMessage());
        }
    }

    @DeleteMapping("/delete/{email}")
    public void deleteDoctor(@PathVariable String email) {
        try {
            service.deleteDoctorByEmail(email);
        } catch (Exception ex) {
            throw new DomainException(ex.getMessage());
        }
    }

    @PostMapping("/bySample")
    public List<Doctor> getDoctorsBySample(@RequestBody DoctorDto dto) {
        return service.getDoctorsBySampleDto(dto);
    }
}
