package com.javapadawan.RegistryServer.web.controller;

import com.javapadawan.RegistryServer.entity.domain.Patient;
import com.javapadawan.RegistryServer.entity.payload.PatientDto;
import com.javapadawan.RegistryServer.exception.DomainException;
import com.javapadawan.RegistryServer.service.logic.PersonManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/logic/patient")
public class PatientController {
    private final PersonManager service;

    @Autowired
    public PatientController(PersonManager service) {
        this.service = service;
    }

    @PostMapping("/save")
    public void addPatient(@RequestBody PatientDto dto,
                           @RequestHeader(name = "Authorization") String authHeader) {
        try {
            service.savePatientFromDto(dto);
            service.registerPatientWithEmail(dto.getEmail(), authHeader);
        } catch (Exception ex) {
            throw new DomainException(ex.getMessage());
        }
    }

    @GetMapping("/getById/{id}")
    public Patient getById(@PathVariable long id) {
        try {
            return service.getPatientById(id);
        } catch (Exception ex) {
            throw new DomainException(ex.getMessage());
        }
    }
}
