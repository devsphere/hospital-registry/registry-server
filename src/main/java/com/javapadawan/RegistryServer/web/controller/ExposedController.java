package com.javapadawan.RegistryServer.web.controller;

import com.javapadawan.RegistryServer.entity.domain.Doctor;
import com.javapadawan.RegistryServer.service.logic.HospitalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/public")
public class ExposedController {
    private final HospitalService service;

    @Autowired
    public ExposedController(HospitalService service) {
        this.service = service;
    }

    @GetMapping("/staff/doctors")
    public List<Doctor> getAllDoctors() {
        return service.getAllDoctors();
    }
}
