package com.javapadawan.RegistryServer.web.controller;

import com.javapadawan.RegistryServer.entity.domain.Person;
import com.javapadawan.RegistryServer.entity.payload.AuthDto;
import com.javapadawan.RegistryServer.service.auth.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/authentication")
public class AuthenticationController {
    private final AuthService service;

    @Autowired
    public AuthenticationController(AuthService service) {
        this.service = service;
    }

    @PostMapping("/byToken")
    public Person authenticate(@RequestBody AuthDto authDto) {
        String role = authDto.getRole();
        try {
            return service.getPerson(authDto.getEmail(), role);
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }
}
