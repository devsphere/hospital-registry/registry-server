package com.javapadawan.RegistryServer.web.controller;

import com.javapadawan.RegistryServer.entity.domain.Referral;
import com.javapadawan.RegistryServer.entity.payload.ReferralDto;
import com.javapadawan.RegistryServer.exception.DomainException;
import com.javapadawan.RegistryServer.service.logic.ReferralManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/logic/referral")
public class ReferralController {
    private final ReferralManager service;

    @Autowired
    public ReferralController(ReferralManager service) {
        this.service = service;
    }

    @GetMapping("/onPatient/{email}")
    public List<Referral> getReferralsOnPatientEmail(@PathVariable String email) {
        return service.getReferralsOnPatientEmail(email);
    }

    @PostMapping("/save")
    public void addReferral(@RequestBody ReferralDto dto) {
        try {
            service.saveReferralFromDto(dto);
        } catch (Exception ex) {
            throw new DomainException(ex.getMessage());
        }
    }

    @DeleteMapping("/delete")
    public void deleteReferral(@RequestBody ReferralDto dto) {
        service.deleteReferralByDto(dto);
    }

    @GetMapping("/onDoctor/{email}")
    public List<Referral> getReferralsOnDoctor(@PathVariable String email) {
        return service.getReferralsOnDoctorEmail(email);
    }
}
