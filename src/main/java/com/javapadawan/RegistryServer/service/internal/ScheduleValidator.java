package com.javapadawan.RegistryServer.service.internal;

import com.javapadawan.RegistryServer.entity.domain.Referral;
import org.springframework.stereotype.Component;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Component
public class ScheduleValidator {
    private final int intervalMin;
    private final int beginningOfWorkDayHr;
    private final int endOfWorkDayHr;

    public ScheduleValidator() {
        intervalMin = 30;
        beginningOfWorkDayHr = 8;
        endOfWorkDayHr = 20;
    }

    public String getValidationListOnReferral(Referral referral) {
        StringBuilder validationList = new StringBuilder();
        LocalDate date = referral.getAppointmentDate();
        LocalTime time = referral.getAppointmentTime();
        if (date.getDayOfWeek().equals(DayOfWeek.SATURDAY) || date.getDayOfWeek().equals(DayOfWeek.SUNDAY)) {
            validationList.append("The chosen date is weekend");
            validationList.append("\n");
        }
        if (time.isAfter(LocalTime.parse(getFormattedHour(endOfWorkDayHr) + ":00"))) {
            validationList.append("Working day ends at " + getFormattedHour(endOfWorkDayHr) + ":00");
            validationList.append("\n");
        }
        if (time.isBefore(LocalTime.parse(getFormattedHour(beginningOfWorkDayHr) + ":00"))) {
            validationList.append("Working day begins at " + getFormattedHour(beginningOfWorkDayHr) + ":00");
            validationList.append("\n");
        }
        if (date.isBefore(LocalDate.now())) {
            validationList.append("Can't select past date");
            validationList.append("\n");
        }
        if (date.equals(LocalDate.now()) && time.isBefore(LocalTime.now())) {
            validationList.append("Can't select past time");
            validationList.append("\n");
        }
        List<Referral> activeReferrals = referral.getDoctor().getReferrals();
        for (Referral ref: activeReferrals) {
            if (date.equals(ref.getAppointmentDate())) {
                if (time.isBefore(ref.getAppointmentTime().plusMinutes(intervalMin))) {
                    validationList.append("The doctor " + getDoctorFullName(referral) + " has another referral at the time");
                    validationList.append("\n");
                    break;
                }
            }
        }
        return validationList.toString();
    }

    private String getDoctorFullName(Referral referral) {
        return referral.getDoctor().getName() + " " + referral.getDoctor().getLastname();
    }

    private String getFormattedHour(int hour) {
        if (hour < 10) {
            return String.format("%02d", hour);
        } else {
            return String.valueOf(hour);
        }
    }
}
