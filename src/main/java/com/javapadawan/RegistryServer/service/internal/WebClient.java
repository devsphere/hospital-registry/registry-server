package com.javapadawan.RegistryServer.service.internal;

import com.javapadawan.RegistryServer.entity.payload.AuthDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

@Service
public class WebClient {
    private final RestTemplate rest;
    private final String authServerAddress;

    public WebClient(@Value("${app.authentication-server.url}") String authServerUrl,
                     @Value("${app.authentication-server.port}") String authServerPort) {
        this.rest = new RestTemplate();
        this.authServerAddress = authServerUrl + authServerPort;
    }

    public AuthDto authorizeUser(String token) {
        try {
            ResponseEntity<AuthDto> response = rest.exchange(
                    authServerAddress + "/authorize/byToken/" + token,
                    HttpMethod.GET,
                    null,
                    AuthDto.class
            );
            return response.getBody();
        } catch (Exception ex) {
            throw new RuntimeException("Invalid token");
        }
    }

    public void requestRegistrationAsPatient(String email, String token) {
        HttpHeaders tokenHeader = new HttpHeaders();
        tokenHeader.add("Authorization", token.substring(7));
        try {
            rest.exchange(
                    authServerAddress + "/registration/patient/" + email,
                    HttpMethod.GET,
                    new HttpEntity<>(tokenHeader),
                    Void.class
            );
        } catch (HttpStatusCodeException ex) {
            throw new RuntimeException(ex.getResponseBodyAs(String.class));
        }
    }
}
