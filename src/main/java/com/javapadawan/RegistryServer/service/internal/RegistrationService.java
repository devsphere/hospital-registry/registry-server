package com.javapadawan.RegistryServer.service.internal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RegistrationService {
    private final WebClient webClient;

    @Autowired
    public RegistrationService(WebClient webClient) {
        this.webClient = webClient;
    }

    public void registerPatientWithEmail(String email, String token) {
        webClient.requestRegistrationAsPatient(email, token);
    }
}
