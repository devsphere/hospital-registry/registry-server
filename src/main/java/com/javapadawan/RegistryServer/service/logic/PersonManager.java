package com.javapadawan.RegistryServer.service.logic;

import com.javapadawan.RegistryServer.entity.domain.Doctor;
import com.javapadawan.RegistryServer.entity.domain.Patient;
import com.javapadawan.RegistryServer.entity.payload.DoctorDto;
import com.javapadawan.RegistryServer.entity.payload.PatientDto;

import java.util.List;

public interface PersonManager {
    void saveDoctorFromDto(DoctorDto dto);
    void deleteDoctorByEmail(String email);
    List<Doctor> getDoctorsBySampleDto(DoctorDto dto);
    List<Doctor> getAllDoctors();
    void savePatientFromDto(PatientDto dto);
    void registerPatientWithEmail(String email, String authToken);
    Patient getPatientById(long id);
    Doctor getDoctorById(long id);
}
