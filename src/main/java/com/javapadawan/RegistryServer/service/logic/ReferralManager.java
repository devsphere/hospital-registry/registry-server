package com.javapadawan.RegistryServer.service.logic;

import com.javapadawan.RegistryServer.entity.domain.Referral;
import com.javapadawan.RegistryServer.entity.payload.ReferralDto;

import java.util.List;

public interface ReferralManager {
    void saveReferralFromDto(ReferralDto dto);
    void deleteReferralByDto(ReferralDto dto);
    List<Referral> getReferralsOnPatientEmail(String email);
    List<Referral> getReferralsOnDoctorEmail(String email);
}
