package com.javapadawan.RegistryServer.service.logic;

import com.javapadawan.RegistryServer.entity.domain.*;
import com.javapadawan.RegistryServer.entity.payload.DoctorDto;
import com.javapadawan.RegistryServer.entity.payload.PatientDto;
import com.javapadawan.RegistryServer.entity.payload.ReferralDto;
import com.javapadawan.RegistryServer.repository.*;
import com.javapadawan.RegistryServer.service.internal.RegistrationService;
import com.javapadawan.RegistryServer.service.internal.ScheduleValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

@Service
public class HospitalService implements ReferralManager, PersonManager {
    private final DoctorRepository doctorRepo;
    private final PatientRepository patientRepo;
    private final ReferralRepository referralRepo;
    private final PersonBasedRepository<Person> personRepo;
    private final PolicyRepository policyRepo;
    private final ScheduleValidator scheduleValidator;
    private final RegistrationService registrationService;

    @Autowired
    public HospitalService(DoctorRepository doctorRepo, PatientRepository patientRepo,
                           ReferralRepository referralRepo, PolicyRepository policyRepo,
                           PersonBasedRepository<Person> personRepo,
                           ScheduleValidator scheduleValidator,
                           RegistrationService registrationService) {
        this.doctorRepo = doctorRepo;
        this.patientRepo = patientRepo;
        this.referralRepo = referralRepo;
        this.personRepo = personRepo;
        this.policyRepo = policyRepo;
        this.scheduleValidator = scheduleValidator;
        this.registrationService = registrationService;
    }

    //public methods
    public List<Doctor> getAllDoctors() {
        return doctorRepo.findAll();
    }

    //logic methods
    public List<Referral> getReferralsOnPatientEmail(String email) {
        return patientRepo.findByEmail(email).getReferrals();
    }

    public List<Referral> getReferralsOnDoctorEmail(String email) {
        return doctorRepo.findByEmail(email).getReferrals();
    }

    public Patient getPatientById(long id) {
        Optional<Patient> patient = patientRepo.findById(id);
        if (patient.isEmpty()) {
            throw new IllegalArgumentException("No user with such id found");
        }
        return patient.get();
    }

    public Doctor getDoctorById(long id) {
        Optional<Doctor> doctor = doctorRepo.findById(id);
        if (doctor.isEmpty()) {
            throw new IllegalArgumentException("No doctor with such id found");
        }
        return doctor.get();
    }

    @Transactional
    public void saveReferralFromDto(ReferralDto dto) {
        Referral referral = extractReferralFromDto(dto);
        String validationList = scheduleValidator.getValidationListOnReferral(referral);
        if (validationList.isEmpty()) {
            referralRepo.save(referral);
        } else {
            throw new IllegalArgumentException(validationList);
        }
    }

    @Transactional
    public void deleteReferralByDto(ReferralDto dto) {
        Referral referral = extractReferralFromDto(dto);
        referralRepo.delete(referral);
    }

    @Transactional
    public void saveDoctorFromDto(DoctorDto dto) {
        Doctor doctor = extractDoctorFromDto(dto);
        String validationList = getValidationList(doctor);
        if (validationList.isEmpty()) {
            doctorRepo.save(doctor);
        } else {
            throw new IllegalArgumentException(validationList);
        }
    }

    @Transactional
    public void deleteDoctorByEmail(String email) {
        Doctor doctor = doctorRepo.findByEmail(email);
        StringBuilder validationList = new StringBuilder();
        if (hasActiveReferrals(doctor)) {
            validationList.append(getFullName(doctor))
                    .append(" has active referrals (")
                    .append(getNumOfActiveReferrals(doctor))
                    .append(")");
            validationList.append("\n");
        }
        if (validationList.isEmpty()) {
            doctorRepo.deleteByEmail(doctor.getEmail());
        } else {
            throw new IllegalArgumentException(validationList.toString());
        }
    }

    @Transactional
    public void savePatientFromDto(PatientDto dto) {
        Patient patient = extractPatientFromDto(dto);
        String validationList = getValidationList(patient);
        if (validationList.isEmpty()) {
            patientRepo.save(patient);
        } else {
            throw new IllegalArgumentException(validationList);
        }
    }

    public void registerPatientWithEmail(String email, String authToken) {
        registrationService.registerPatientWithEmail(email, authToken);
    }

    public List<Doctor> getDoctorsBySampleDto(DoctorDto dto) {
        Doctor sample = extractDoctorFromDto(dto);
        Example<Doctor> example = Example.of(sample, ExampleMatcher.matchingAny());
        return doctorRepo.findAll(example);
    }

    //private methods
    private Patient extractPatientFromDto(PatientDto dto) {
        Patient patient = new Patient();
        patient.setName(dto.getName());
        patient.setLastname(dto.getLastname());
        patient.setEmail(dto.getEmail());
        patient.setEmployment(extractEmployment(dto.getEmployment()));
        patient.setBirthDate(LocalDate.parse(dto.getBirthDate()));
        patient.setPolicy(generatePolicy());
        return patient;
    }

    private Employment extractEmployment(String employment) {
        return switch (employment) {
            case "STUDENT" -> Employment.STUDENT;
            case "RETIRED" -> Employment.RETIRED;
            case "EMPLOYED" -> Employment.EMPLOYED;
            case "UNEMPLOYED" -> Employment.UNEMPLOYED;
            default -> Employment.UNKNOWN;
        };
    }

    private InsurancePolicy generatePolicy() {
        InsurancePolicy policy = new InsurancePolicy();
        LocalDate now = LocalDate.now();
        policy.setRegistrationDate(now);
        policy.setExpirationDate(now.plusYears(10));
        policy.setCode(generatePolicyIdCode());
        return policy;
    }

    private String generatePolicyIdCode() {
        int currentYear = LocalDate.now().getYear();
        InsurancePolicy previous = policyRepo.findTheLastOne();
        int codePostfix = Integer.parseInt(previous.getCode().substring(8));
        codePostfix = codePostfix + 1;
        String formattedPostfix = String.format("%04d", codePostfix);
        String code = "";
        code += currentYear;
        code += "-01-";
        code += formattedPostfix;
        return code;
    }

    private Referral extractReferralFromDto(ReferralDto dto) {
        Referral referral = new Referral();
        Doctor doctor = (Doctor) getPersonByEmail(dto.getDoctorEmail());
        Patient patient = (Patient) getPersonByEmail(dto.getPatientEmail());
        referral.setDoctor(doctor);
        referral.setPatient(patient);
        LocalDate date = LocalDate.parse(dto.getDate());
        referral.setAppointmentDate(date);
        LocalTime time = LocalTime.parse(dto.getTime());
        referral.setAppointmentTime(time);
        return referral;
    }

    private Person getPersonByEmail(String email) {
        return personRepo.findByEmail(email);
    }

    private Doctor extractDoctorFromDto(DoctorDto dto) {
        Doctor doctor = new Doctor();
        if (dto.getEmail() == null) {
            doctor.setEmail("");
        }
        doctor.setEmail(dto.getEmail());
        if (dto.getName() == null) {
            doctor.setName("");
        }
        doctor.setName(dto.getName());
        if (dto.getLastname() == null) {
            doctor.setLastname("");
        }
        doctor.setLastname(dto.getLastname());
        if (dto.getSpecialization() == null) {
            doctor.setSpecialization("");
        }
        doctor.setSpecialization(dto.getSpecialization());
        doctor.setCabinet(dto.getCabinet());
        try {
            LocalDate birthDate = LocalDate.parse(dto.getBirthDate());
            doctor.setBirthDate(birthDate);
        } catch (Exception ex) {
            doctor.setBirthDate(LocalDate.now());
        }
        try {
            LocalDate employmentDate = LocalDate.parse(dto.getEmploymentDate());
            doctor.setEmploymentDate(employmentDate);
        } catch (Exception ex) {
            doctor.setEmploymentDate(LocalDate.now());
        }
        return doctor;
    }

    private String getValidationList(Doctor doctor) {
        StringBuilder validationList = new StringBuilder();
        if (doctor.getEmail().isEmpty()) {
            validationList.append("The email is empty");
            validationList.append("\n");
        }
        if (doctor.getName().isEmpty()) {
            validationList.append("The name is empty");
            validationList.append("\n");
        }
        if (doctor.getLastname().isEmpty()) {
            validationList.append("The lastname is empty");
            validationList.append("\n");
        }
        if (doctor.getSpecialization().isEmpty()) {
            validationList.append("The specialization is empty");
            validationList.append("\n");
        }
        if (isEmailRegistered(doctor.getEmail())) {
            validationList.append("The email is taken");
            validationList.append("\n");
        }
        if (isCabinetTaken(doctor.getCabinet())) {
            validationList.append("The cabinet is taken");
            validationList.append("\n");
        }
        return validationList.toString();
    }

    private String getValidationList(Patient patient) {
        StringBuilder validationList = new StringBuilder();
        if (patient.getEmail().isEmpty()) {
            validationList.append("The email is empty");
            validationList.append("\n");
        }
        if (patient.getName().isEmpty()) {
            validationList.append("The name is empty");
            validationList.append("\n");
        }
        if (patient.getLastname().isEmpty()) {
            validationList.append("The lastname is empty");
            validationList.append("\n");
        }
        if (isEmailRegistered(patient.getEmail())) {
            validationList.append("The email is taken");
            validationList.append("\n");
        }
        return validationList.toString();
    }

    private boolean isEmailRegistered(String email) {
        return personRepo.existsByEmail(email);
    }

    private boolean isCabinetTaken(int number) {
        return doctorRepo.existsByCabinet(number);
    }

    private String getFullName(Doctor doctor) {
        return doctor.getName() + " " + doctor.getLastname();
    }

    private boolean hasActiveReferrals(Doctor doctor) {
        List<Referral> activeReferrals = doctor.getReferrals();
        if (activeReferrals == null) {
            return false;
        } else {
            return !(activeReferrals.isEmpty());
        }
    }

    private int getNumOfActiveReferrals(Doctor doctor) {
        return doctor.getReferrals().size();
    }
}
