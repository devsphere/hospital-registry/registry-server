package com.javapadawan.RegistryServer.service.auth;

import com.javapadawan.RegistryServer.entity.auth.Credential;
import com.javapadawan.RegistryServer.entity.auth.RegistryUser;
import com.javapadawan.RegistryServer.entity.domain.Person;
import com.javapadawan.RegistryServer.entity.payload.AuthDto;
import com.javapadawan.RegistryServer.repository.DoctorRepository;
import com.javapadawan.RegistryServer.repository.ManagerRepository;
import com.javapadawan.RegistryServer.repository.PatientRepository;
import com.javapadawan.RegistryServer.service.internal.WebClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

@Service
public class AuthService implements UserDetailsService {
    private final WebClient webClient;
    private final PatientRepository patientRepo;
    private final DoctorRepository doctorRepo;
    private final ManagerRepository managerRepo;

    @Autowired
    public AuthService(WebClient webClient,
                       PatientRepository patientRepo,
                       DoctorRepository doctorRepo,
                       ManagerRepository managerRepo) {
        this.webClient = webClient;
        this.patientRepo = patientRepo;
        this.doctorRepo = doctorRepo;
        this.managerRepo = managerRepo;
    }

    @Override
    public UserDetails loadUserByUsername(String token) {
        AuthDto dto = webClient.authorizeUser(token);
        return new RegistryUser(new Credential(dto.getEmail(), dto.getPassHash()), dto.getRole());
    }

    public Person getPerson(String email, String role) {
        return switch (role) {
            case "PATIENT" -> patientRepo.findByEmail(email);
            case "DOCTOR" -> doctorRepo.findByEmail(email);
            case "ADMIN" -> managerRepo.findByEmail(email);
            default -> throw new IllegalArgumentException("Enable to find email bearer");
        };
    }
}
