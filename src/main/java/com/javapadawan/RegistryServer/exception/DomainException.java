package com.javapadawan.RegistryServer.exception;

public class DomainException extends RuntimeException {
    public DomainException(String message) {
        super(message);
    }
}
