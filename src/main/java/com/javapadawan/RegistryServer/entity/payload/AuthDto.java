package com.javapadawan.RegistryServer.entity.payload;

public class AuthDto {
    private String email;
    private String role;
    private String passHash;

    public AuthDto() {
    }

    public AuthDto(String email, String role, String passHash) {
        this.email = email;
        this.role = role;
        this.passHash = passHash;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getPassHash() {
        return passHash;
    }

    public void setPassHash(String passHash) {
        this.passHash = passHash;
    }
}
