package com.javapadawan.RegistryServer.entity.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.List;

@Entity(name = "patient")
public class Patient extends Person {
    @Column(name = "employment")
    @Enumerated(EnumType.STRING)
    private Employment employment;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "policy_id", referencedColumnName = "id")
    private InsurancePolicy policy;
    @JsonIgnore
    @OneToMany(mappedBy = "patient")
    private List<Referral> referrals;

    public Patient() {
    }

    public List<Referral> getReferrals() {
        return referrals;
    }

    public void setReferrals(List<Referral> referrals) {
        this.referrals = referrals;
    }

    public Employment getEmployment() {
        return employment;
    }

    public void setEmployment(Employment employment) {
        this.employment = employment;
    }

    public InsurancePolicy getPolicy() {
        return policy;
    }

    public void setPolicy(InsurancePolicy policy) {
        this.policy = policy;
    }
}
