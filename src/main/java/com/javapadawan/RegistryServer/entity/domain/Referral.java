package com.javapadawan.RegistryServer.entity.domain;

import jakarta.persistence.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Objects;

@Entity(name = "referral")
@IdClass(ReferralKey.class)
public class Referral {
    @Id
    @ManyToOne
    @JoinColumn(name = "doctor_id", referencedColumnName = "id")
    private Doctor doctor;
    @Id
    @ManyToOne
    @JoinColumn(name = "patient_id", referencedColumnName = "id")
    private Patient patient;
    @Id
    @Column(name = "appointment_date")
    private LocalDate appointmentDate;
    @Id
    @Column(name = "appointment_time")
    private LocalTime appointmentTime;

    public Referral() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Referral referral = (Referral) o;
        return doctor.equals(referral.doctor) &&
                patient.equals(referral.patient) &&
                appointmentDate.equals(referral.appointmentDate) &&
                appointmentTime.equals(referral.appointmentTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(doctor, patient, appointmentDate, appointmentTime);
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public LocalDate getAppointmentDate() {
        return appointmentDate;
    }

    public void setAppointmentDate(LocalDate appointmentDate) {
        this.appointmentDate = appointmentDate;
    }

    public LocalTime getAppointmentTime() {
        return appointmentTime;
    }

    public void setAppointmentTime(LocalTime appointmentTime) {
        this.appointmentTime = appointmentTime;
    }
}
