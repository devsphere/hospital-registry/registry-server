package com.javapadawan.RegistryServer.entity.domain;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;

import java.time.LocalDate;

@Entity(name = "manager")
public class Manager extends Person {
    @Column(name = "employment_date")
    private LocalDate employmentDate;
    @Column(name = "department")
    @Enumerated(EnumType.STRING)
    private Department department;

    public Manager() {
    }

    public LocalDate getEmploymentDate() {
        return employmentDate;
    }

    public void setEmploymentDate(LocalDate employmentDate) {
        this.employmentDate = employmentDate;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }
}
