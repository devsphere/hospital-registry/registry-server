package com.javapadawan.RegistryServer.entity.domain;

public enum Employment {
    STUDENT,
    EMPLOYED,
    RETIRED,
    UNEMPLOYED,
    UNKNOWN
}
