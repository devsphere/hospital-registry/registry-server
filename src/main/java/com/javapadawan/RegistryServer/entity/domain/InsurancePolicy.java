package com.javapadawan.RegistryServer.entity.domain;

import jakarta.persistence.*;

import java.time.LocalDate;

@Entity(name = "insurance_policy")
public class InsurancePolicy {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;
    @Column(name = "code") // xxxx-xx-xxxx
    private String code;
    @Column(name = "registration_date")
    private LocalDate registrationDate;
    @Column(name = "expiration_date")
    private LocalDate expirationDate;

    public InsurancePolicy() {
    }

    public InsurancePolicy(long id, String code, LocalDate registrationDate, LocalDate expirationDate) {
        this.id = id;
        this.code = code;
        this.registrationDate = registrationDate;
        this.expirationDate = expirationDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public LocalDate getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(LocalDate registrationDate) {
        this.registrationDate = registrationDate;
    }

    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(LocalDate expirationDate) {
        this.expirationDate = expirationDate;
    }
}
