package com.javapadawan.RegistryServer.entity.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@Entity(name = "doctor")
public class Doctor extends Person {
    @Column(name = "employment_date")
    private LocalDate employmentDate;
    @Column(name = "specialization")
    private String specialization;
    @Column(name = "cabinet")
    private int cabinet;
    @JsonIgnore
    @OneToMany(mappedBy = "doctor")
    private List<Referral> referrals;

    public Doctor() {
    }

    public List<Referral> getReferrals() {
        return referrals;
    }

    public void setReferrals(List<Referral> referrals) {
        this.referrals = referrals;
    }

    public LocalDate getEmploymentDate() {
        return employmentDate;
    }

    public void setEmploymentDate(LocalDate employmentDate) {
        this.employmentDate = employmentDate;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public int getCabinet() {
        return cabinet;
    }

    public void setCabinet(int cabinet) {
        this.cabinet = cabinet;
    }
}
