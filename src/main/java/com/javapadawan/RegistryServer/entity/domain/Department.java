package com.javapadawan.RegistryServer.entity.domain;

public enum Department {
    HR,
    SMM,
    ADMINISTRATION
}
