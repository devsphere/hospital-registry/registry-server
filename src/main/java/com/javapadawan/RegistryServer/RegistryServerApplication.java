package com.javapadawan.RegistryServer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@SpringBootApplication
@ConfigurationPropertiesScan
public class RegistryServerApplication {
	public static void main(String[] args) {
		SpringApplication.run(RegistryServerApplication.class, args);
	}
}
