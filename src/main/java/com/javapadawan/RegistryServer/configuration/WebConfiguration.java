package com.javapadawan.RegistryServer.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Component
@Configuration
public class WebConfiguration implements WebMvcConfigurer {
    private final String allowedIOSOrigin;
    private final String allowedWebOrigin;

    public WebConfiguration(@Value("${app.web-bff-server.url}") String webUrl,
                            @Value("${app.web-bff-server.port}") String webPort,
                            @Value("${app.ios-bff-server.url}") String iosUrl,
                            @Value("${app.ios-bff-server.port}") String iosPort) {
        this.allowedIOSOrigin = iosUrl + iosPort;
        this.allowedWebOrigin = webUrl + webPort;
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins(
                        "http://localhost:9099",
                        "http://localhost:9100",
                        allowedIOSOrigin,
                        allowedWebOrigin
                )
                .allowedMethods("GET", "POST", "PUT", "DELETE");
    }
}
